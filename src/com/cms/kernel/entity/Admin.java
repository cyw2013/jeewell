package com.cms.kernel.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Copyright (C) 2013 2016 Well
 * 文件: Admin.java
 * 创建: Well
 * 日期: 2016年5月25日 上午11:23:28
 * 来自: 0.9.0
 * 版本: 0.9.0
 * 描述: 后台管理员的实体类，
 */

@Entity
@Table(name = "t_admin")
public class Admin extends ABaseEntity
{

	@Column(name = "username",nullable=false)
	private String username; //用户名

	@Column(name = "password",nullable=false)
	private transient String password; //密码

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
}
