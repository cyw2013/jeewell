package com.cms.kernel.shiro.freemarker;

import com.cms.kernel.shiro.freemarker.tag.ShiroFreemarkerTag;
import freemarker.template.TemplateException;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.IOException;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:ShiroFreemarkerConfigurer.java
 * 创建:Well
 * 日期:2016年6月23日 下午2:17:50
 * 来自:
 * 版本:
 * 描述:自定义在freemarker模板中shiro的标签方法
 */

public class ShiroFreemarkerConfigurer extends FreeMarkerConfigurer
{
    @Override
    public void afterPropertiesSet() throws IOException, TemplateException
    {
        super.afterPropertiesSet();
        this.getConfiguration().setSharedVariable("shiro",new ShiroFreemarkerTag());
    }
}
