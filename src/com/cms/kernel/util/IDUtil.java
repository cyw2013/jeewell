package com.cms.kernel.util;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:IDUtil.java
 * 创建:Well
 * 日期:2016年5月25日 上午11:52:49
 * 来自:
 * 版本:1.0.0
 * 描述:主键ID的利用类
 */

public class IDUtil
{
	/**
	 * 生成UUID类型的主键，长度为32，主键中不存在 "-" 这个字符
	 * @return
	 */
	public static String generate()
	{
		return StringUtils.replace(generateUUID(), "-", "");
	}
	
    /**
     * 生成UUID类型的主键，长度为36，主键中带有 "-" 这个字符
     * @return 主键字符串
     */
    public static String generateUUID()
    {
        return UUID.randomUUID().toString();
    }
}
