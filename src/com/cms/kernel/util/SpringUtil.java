package com.cms.kernel.util;

import org.springframework.context.ApplicationContext;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:SpringUtil.java
 * 创建:Well
 * 日期:2016年5月25日 下午3:09:54
 * 来自:
 * 版本:0.9.0
 * 描述:spring的util类
 * 		编写测试用例时，获取注入在spring中的bean
 */

public class SpringUtil
{
	/* spring配置文件 */
	private static final String SPRING_CONFIG = "com/config/spring.xml";
	
	/* 获取spring的上下文 */
	private static ApplicationContext applicationContext = new ClassPathXmlApplicationContext(SPRING_CONFIG);

	/**
	 * 获取对应类型的bean
	 * @param clazz 类型
	 * @return bean
	 */
	public static <T> T getBean(Class<T> clazz)
	{
		return applicationContext.getBean(clazz);
	}

	/**
	 * 获取spring中指定bean名称的bean
	 * @param name bean名称
	 * @return bean
	 */
	public static Object getBean(String name)
	{
		return applicationContext.getBean(name);
	}
}
