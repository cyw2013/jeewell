package com.cms.common.mapper;

import java.util.List;

import com.cms.kernel.entity.User;

import tk.mybatis.mapper.common.Mapper;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:UserMapper.java
 * 创建:Well
 * 日期:2016年5月27日 上午10:38:44
 * 来自:
 * 版本:
 * 描述:用户的mapper接口
 */

public interface UserMapper extends Mapper<User>
{
	List<User> selectUserList();
}
