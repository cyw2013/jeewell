package com.cms.common.mapper;

import com.cms.kernel.entity.UserDetail;

import tk.mybatis.mapper.common.Mapper;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:UserDetailMapper.java
 * 创建:Well
 * 日期:2016年6月4日 上午9:41:15
 * 来自:
 * 版本:
 * 描述:用户详细信息的mapper接口
 */

public interface UserDetailMapper extends Mapper<UserDetail>
{

}
