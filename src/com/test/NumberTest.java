package com.test;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:NumberTest.java
 * 创建:Well
 * 日期:2016年5月26日 下午1:27:23
 * 来自:
 * 版本:
 * 描述:
 */

public class NumberTest
{
	private Logger logger=LogManager.getLogger(NumberTest.class);
	
	@Test
	public void test()
	{
		BigInteger bigInteger=NumberUtils.createBigInteger("12345678901234567890");
		logger.info("bigInteger为{}",bigInteger);
	}
	
	@Test
	public void test2()
	{
		BigDecimal bigDecimal=NumberUtils.createBigDecimal("381.19237");
		System.out.println(bigDecimal);
	}
	
}
