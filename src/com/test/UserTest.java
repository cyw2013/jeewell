package com.test;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.cms.admin.service.UserService;
import com.cms.kernel.entity.User;
import com.cms.kernel.util.SpringUtil;

/**
 * Copyright (C) 2013 2016 Well
 * 文件:UserTest.java
 * 创建:Well
 * 日期:2016年5月27日 上午9:56:49
 * 来自:
 * 版本:
 * 描述:用户的测试用例
 */

public class UserTest
{
	private static Logger logger=LogManager.getLogger(UserTest.class);
	
	private UserService userService=SpringUtil.getBean(UserService.class);
	
	/**
	 * 测试新增用户
	 */
	@Test
	public void testInsertUser()
	{
		String username="1";
		String password="one";
		int result=userService.insertUser(username,password);
		if(result>0)
		{
			logger.info("新增user成功！");
		}
		else
		{
			logger.info("新增user失败！");
		}
	}
	
	/**
	 * 测试查询所有用户
	 */
	@Test
	public void testSelectUserList()
	{
		List<User> userList=userService.selectList();
		if(userList.size()==0)
		{
			logger.warn("没有查询到用户");
		}
		else
		{
			for(User user:userList)
			{
				logger.info(user.getId()+"	"+user.getUsername()+"	"+user.getPassword());
			}
		}
	}
	
	/**
	 * 测试新增user和userDetail
	 */
	@Test
	public void testInsertUserAndUserDetail()
	{
		int result=userService.insertUser();
		logger.info("result="+result);
	}
	
	/**
	 * 测试更新用户
	 */
	@Test
	public void testUpdateUser()
	{
		String id="397e3118c0384e03ab7c03036f848da6";
		String password="oneone";
		
		User user=new User();
		user.setId(id);
		user.setPassword(password);
		
		int result=userService.updateSelective(user);
		if(result>0)
		{
			logger.info("修改user成功！");
		}
		else
		{
			logger.info("修改user失败！");
		}
	}
	
	/**
	 * 测试删除用户
	 */
	@Test
	public void testDeleteUser()
	{
		String id="cdee31ec3d0d47ddbd7ceda7d15c2aba";
		
		int result=userService.delete(id);
		if(result>0)
		{
			logger.info("删除user成功！");
		}
		else
		{
			logger.info("删除user失败！");
		}
		
	}
	
	/**
	 * 测试删除用户列表
	 */
	@Test
	public void testDeleteUserList()
	{
		List<String> idList=new ArrayList<String>();
		idList.add("01163c79a7ad4c2cba61c871b964c9a8");
		idList.add("0350f8a202344d18aafcae51c2274321");
		idList.add("07ad9d42d1c84a0fb543d773f5adbfb7");
		idList.add("416ade5acf44468a9184533b36e73d56");
		idList.add("a4b96b3be291452ea7353417913d18ee");
		
		int result=userService.deleteList(idList);
		if(result>0)
		{
			logger.info("删除【"+result+"】个user成功！");
		}
		else
		{
			logger.info("删除user失败！");
		}
	}
	
}
