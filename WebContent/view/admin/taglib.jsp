<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!-- 项目的根路径 -->
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<!-- 资源的根路径 -->
<c:set var="ctxAsset" value="${pageContext.request.contextPath}/asset" />

